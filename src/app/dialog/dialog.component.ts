import { Component ,Inject,OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements  OnInit {
 freshnessList =['Brand New','Second hand','Refurbished']
 actionBtn:string="Save"
 /* why is there an exclamation mark here */
/*  what is this ngoninit and why do we need a constructor */
productForm!:FormGroup
  dialogRef: any;
  constructor(private formBuilder :FormBuilder , private api : ApiService,@Inject(MAT_DIALOG_DATA) public editData:any,public dialogRef1: MatDialogRef<DialogComponent>) { 
    this.dialogRef = dialogRef1;
  }  
/* form validation */
  ngOnInit():void {
    this.productForm = this.formBuilder.group({
      productName:['',Validators.required],
      category:['',Validators.required],
      startdate:['',Validators.required],
      enddate:['',Validators.required],
      freshness:['',Validators.required],
      price:['',Validators.required],
      comment:['',Validators.required]
      
    })
  if(this.editData){
    this.actionBtn="Update"
    this.productForm.controls['productName'].setValue(this.editData.productName);
    this.productForm.controls['category'].setValue(this.editData.category);
    this.productForm.controls['startdate'].setValue(this.editData.startdate);
    this.productForm.controls['enddate'].setValue(this.editData.enddate);
    this.productForm.controls['freshness'].setValue(this.editData.freshness);
    this.productForm.controls['price'].setValue(this.editData.price);
    this.productForm.controls['comment'].setValue(this.editData.comment);

  }
  
  }
  addProduct(){
    if(!this.editData){
      if(this.productForm.valid){
        this.api.postProduct(this.productForm.value).subscribe({
          next:(res)=>{
            alert("product added successfully")
            this.productForm.reset();
            this.dialogRef.close('save');
          },
          error:()=>{
            alert("something went wrong")
          },
          })
        }
      }
        else{
          this.updateProduct()
}
    
  
  }
  updateProduct(){
this.api.putProduct(this.productForm.value,this.editData.id).subscribe({
  next:(res)=> {
    alert("product updated successfully");
    this.productForm.reset();
    this.dialogRef.close('update');
  },
  error:()=>{
    alert("something went wrong");
  }
  })


}

}
