import { Component ,OnInit ,ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ApiService } from './services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { HttpClient } from '@angular/common/http';
import { D3Component } from './d3/d3.component';
import { read } from 'xlsx';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular13Crud';
  userInfo!: any;
  data1!: any;
 /*  data = [
    {
      name: "Test 1",
      age: 13,
      average: 8.2,
      approved: true,
      description: "using 'Content here, content here' "
    },
    {
      name: 'Test 2',
      age: 11,
      average: 8.2,
      approved: true,
      description: "using 'Content here, content here' "
    },
    {
      name: 'Test 4',
      age: 10,
      average: 8.2,
      approved: true,
      description: "using 'Content here, content here' "
    },
  ]; */
  fileDownload(){
    var abcd = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Report Data',
      useBom: true,
     
      headers: ['productName', 'category', 'startdate','enddate', 'freshness','price','comment']
    };
   
    new ngxCsv(this.userInfo, "Report", abcd);
  }

  displayedColumns: string[] = ['productName', 'category', 'startdate','enddate', 'freshness','price','comment','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(public dialog: MatDialog , private api : ApiService,private http:HttpClient) {

  }
  ngOnInit():void{
    const url:string ='./db.json'
    this.http.get(url).subscribe((response)=>{
this.userInfo=response;
    });
this.getAllproducts();

  }
  
  openDialog() {
    this.dialog.open(DialogComponent,{
      width:'30%'
    }).afterClosed().subscribe(val=>{
      if(val==='save'){
        this.getAllproducts()
      }
    });
  }
  
  getAllproducts(){
    this.api.getProduct().subscribe({
      next:(res)=>{
        this.dataSource=new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort=this.sort;
       this.userInfo=res
      },
      error:(err)=>{
        alert("Error while fetching data")
      }
    })
  }
  editProduct(row:any){
    this.dialog.open(DialogComponent,{
      width:"30%",
      data:row
    }).afterClosed().subscribe(val=>{
      if(val==='update'){
        this.getAllproducts();
      }
    })
  }
 
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  deleteProduct(id:number){
this.api.deleteProduct(id)
.subscribe({
  next:(res)=>{
    alert("Product deleted successfully")
    this.getAllproducts()
  },
  error:()=>{
    alert("Error while deleting the record")
  }
})
  }
  fileRead(event:any){
    const file = event.target.files[0];
const reader = new FileReader();

reader.onload = (e: any) => {
  const data = e.target.result;
  const workbook = read(data, { type: 'binary' });
  const sheetName = workbook.SheetNames[0];
  const worksheet = workbook.Sheets[sheetName];
  const jsonData = JSON.stringify(XLSX.utils.sheet_to_json(worksheet));
  this.data1=jsonData;
};

reader.readAsBinaryString(file);

  }
  
}
